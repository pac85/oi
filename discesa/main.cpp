#include <bits/stdc++.h>

using namespace std;

int main()
{
    ifstream inf("input.txt");
    ofstream outf("output.txt");

    int A;
    vector<int> piramide[10];

    inf >> A;
    for(int i = 0;i < A;i++)
    {
        piramide[i].resize(i+1);
        for(int j = 0;j <= i;j++)
        {
            inf >> piramide[i][j];
        }
    }

    for(int i = A-2;i >= 0;i--)
    {
        for(int j = 0;j <= i;j++)
        {
            piramide[i][j] += max(piramide[i+1][j], piramide[i+1][j+1]);
        }
    }

    outf << piramide[0][0] << '\n';
}
