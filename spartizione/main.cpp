#include <bits/stdc++.h>

using namespace std;

int main()
{
    ifstream inf("input.txt");
    ofstream outf("output.txt");

    int G, P;
    inf >> G >> P;
    P--;
    int sol = 0;
    int  g = G;

    for(int i = 1;g > (i+P);i++)
    {
        g -= i+P;
        sol += i;
    }
    sol += g;

    outf << sol<< '\n';
}
