#include <bits/stdc++.h>

using namespace std;

#define MAX_N 100000

int sg[4*MAX_N];
bitset<4*MAX_N> lazy;

#define sx(i) 2*i+1
#define dx(i) 2*i+2

inline int range_m(int a, int b)
{
    return (a+b)/2;
}

inline bool opxor(bool a, bool b)
{
    return a?!b:b;
}

void update(int root, int A, int B, int l, int r)
{
    if(B < l || r < A)return;
    //if(A <= l && r <= B) {lazy[root] = lazy[root]; return;}
    if(l==r) {sg[root] = !sg[root]; return;}

    int m = range_m(l, r);
    update(sx(root), A, B, l, m);
    update(dx(root), A, B, m+1, r);
    sg[root] = sg[sx(root)] + sg[dx(root)];
}

int query(int root, int A, int B, int l, int r, bool flipped = lazy[0])
{
    if(A <= l && r <= B) return opxor(flipped, sg[root]);
    if(B < l || r < A) return 0;

    int m = range_m(l, r);
    return  query(sx(root), A, B, l, m, opxor(flipped, lazy[sx(root)])) +
            query(dx(root), A, B, m+1, r, opxor(flipped, lazy[dx(root)]));
}

int main()
{
    ifstream inf("input.txt");
    ofstream outf("output.txt");

    int N, Q;
    inf >> N >> Q;

    for (int i = 0;i < Q; i++)
    {
        bool qtype;
        int A, B;
        inf >> qtype >> A >> B;
        if(!qtype)
            update(0, A, B, 0, N-1);
        else
            outf << query(0, A, B, 0, N-1) << endl;
    }
}
