#include <bits/stdc++.h>

using namespace std;

vector<int> solve()
{

}

int main()
{
    ifstream inf("input.txt");
    ofstream outf("output.txt");

    int T;
    inf >> T;

    for(int i = 0;i < T;i++)
    {
        int N;
        inf >> N;
        vector<int> c(N);
        for(int j = 0;j < N;j++)
        {
            inf >> c[j];
        }
        cout << "Case #" << i << ": " << endl;
        for(auto sc : solve())
            cout << sc << " ";
        cout << endl;
    }
}
