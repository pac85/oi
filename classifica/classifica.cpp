#include<cstring>
#include<bits/stdc++.h>

using namespace std;

#define MAX_N 1000000

#define sx(i) i*2+1
#define dx(i) i*2+2

int ids[MAX_N];
int reverse_id[MAX_N];
int sg[MAX_N*4];
int _prev[MAX_N], _next[MAX_N];

int N;

int build(int root, int l, int r)
{
    if(l==r) return sg[root] = 1;
    int m = (l+r)/2;
    return sg[root] = build(sx(root), l, m) + build(dx(root), m+1, r);
}

void update(int root, int pos, int l, int r)
{
    if(l > pos || r < pos) return;
    if(l==r){ sg[root] = 0; return;}

    int m = (l+r)/2;
    update(sx(root), pos, l, m);
    update(dx(root), pos, m+1, r);
    sg[root] = sg[sx(root)] + sg[dx(root)];
}

inline int mid(int l, int r){return (l+r)/2;}

int search_one(int root, int l, int r)
{
    if(l==r) return l;

    if(sg[sx(root)])
        return search_one(sx(root), l, mid(l, r));
    else
        return search_one(dx(root), mid(l, r)+1, r);

}

int query(int root, int pos, int l, int r)
{
    if(sg[root] == 1) return search_one(root, l, r);

    if(sg[sx(root)] > pos)
        return query(sx(root), pos, l, mid(l, r));
    else
        return query(dx(root), pos-sg[sx(root)], mid(l, r)+1, r);
}

void inizia(int _N, int _ids[]) {
    N = _N;
    memcpy(ids, _ids, sizeof(int)*N);
    for(int i = 0;i < N;i++)
    {
        _prev[ids[i]] = ids[i+1];
        _next[ids[i]] = ids[i-1];
        reverse_id[ids[i]] = i;
    }

    build(0, 0, N-1) ;
}

void supera(int id) {
    swap(ids[reverse_id[id]], ids[reverse_id[_next[id]]]);
    swap(reverse_id[id], reverse_id[_next[id]]);
    swap(_prev[_next[id]], _prev[id]);
    swap(_next[id], _next[_next[id]]);
}

void squalifica(int id) {
    update(0, reverse_id[id], 0, N-1);
    _next[_prev[id]] = _next[id];
    _prev[_next[id]] = _prev[id];
}

int partecipante(int pos) {
    return ids[query(0, pos, 0, N-1)];
}

